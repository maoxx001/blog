import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: '',
    userInfo: sessionStorage.getItem('userInfo') ? JSON.parse(sessionStorage.getItem('userInfo')) : {}
  },
  getters: {
    // java 中的

    GET_INFO(state) { // 获取各类信息
      return state.userInfo
    }
  },
  mutations: {
    // java 中的 set方法

    SET_TOKEN(state, token) { // 设置token
      state.token = token;
      localStorage.setItem('token', token);
    },
    SET_USERINFO(state, userInfo) { // 设置用户信息
      state.userInfo = userInfo;
      sessionStorage.setItem('userInfo', JSON.stringify(userInfo));
    },
    REMOVE_INFO(state) { // 移除各类信息
      state.token = '';
      state.userInfo = {};
      localStorage.setItem('token', '');
      sessionStorage.setItem('userInfo', JSON.stringify(''));
    }
  },
  actions: {
  },
  modules: {
  }
})
