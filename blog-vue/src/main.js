import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import './axios'

// 全局注册
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'



// 引入axios,并配置
import axios from 'axios'
Vue.prototype.$axios = axios;


// 引入element-ui,并配置
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'


// use
Vue.use(mavonEditor)
Vue.use(ElementUI)


Vue.config.productionTip = false // 关闭生产模式下的提示



new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
