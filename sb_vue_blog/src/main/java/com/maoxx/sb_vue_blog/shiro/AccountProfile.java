package com.maoxx.sb_vue_blog.shiro;

import lombok.Data;

import java.io.Serializable;


// 可暴露给外界用户信息
@Data
public class AccountProfile implements Serializable {

    private Long id;

    private String username;

    private String avatar;

    private String email;

}