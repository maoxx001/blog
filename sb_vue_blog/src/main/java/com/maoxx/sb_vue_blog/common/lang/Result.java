package com.maoxx.sb_vue_blog.common.lang;


import lombok.Data;

import java.io.Serializable;

@Data
public class Result implements Serializable {
    private String code;
    private String msg;
    private Object data;

    public Result(String code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static Result success(Object data) {
        return new Result("200", "操作成功", data);
    }

    public static Result success(String msg, Object data) {
        return new Result("200", msg, data);
    }

    public static Result fail(String msg) {
        return new Result("500", msg, null);
    }

    public static Result fail(String msg, Object data) {
        return new Result("500", msg, data);
    }
    public static Result fail(String code, String msg, Object data) {
        return new Result(code, msg, data);
    }
}
