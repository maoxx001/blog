package com.maoxx.sb_vue_blog.service;

import com.maoxx.sb_vue_blog.entity.Blog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 关注公众号：MarkerHub
 * @since 2024-07-05
 */
public interface BlogService extends IService<Blog> {

}
