package com.maoxx.sb_vue_blog.service.impl;

import com.maoxx.sb_vue_blog.entity.Blog;
import com.maoxx.sb_vue_blog.mapper.BlogMapper;
import com.maoxx.sb_vue_blog.service.BlogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 关注公众号：MarkerHub
 * @since 2024-07-05
 */
@Service
public class BlogServiceImpl extends ServiceImpl<BlogMapper, Blog> implements BlogService {

}
