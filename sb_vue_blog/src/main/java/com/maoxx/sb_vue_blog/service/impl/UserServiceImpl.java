package com.maoxx.sb_vue_blog.service.impl;

import com.maoxx.sb_vue_blog.entity.User;
import com.maoxx.sb_vue_blog.mapper.UserMapper;
import com.maoxx.sb_vue_blog.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 关注公众号：MarkerHub
 * @since 2024-07-05
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
