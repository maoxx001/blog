package com.maoxx.sb_vue_blog.controller;


import com.maoxx.sb_vue_blog.common.lang.Result;
import com.maoxx.sb_vue_blog.entity.User;
import com.maoxx.sb_vue_blog.service.UserService;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 关注公众号：MarkerHub
 * @since 2024-07-05
 */
@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

    @Autowired
    private UserService userService;


    @RequiresAuthentication
    @GetMapping("/index")
    public Result index() {
        return Result.success(userService.getById(1L));
    }

    @PostMapping("/save")
    public Result save(@Validated @RequestBody User user) {
        return Result.success(userService.save(user));
    }

}
