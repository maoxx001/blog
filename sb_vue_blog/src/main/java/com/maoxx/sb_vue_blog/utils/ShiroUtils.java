package com.maoxx.sb_vue_blog.utils;

import com.maoxx.sb_vue_blog.shiro.AccountProfile;
import org.apache.shiro.SecurityUtils;

public class ShiroUtils {
    public static AccountProfile getProfile(){
        return (AccountProfile) SecurityUtils.getSubject().getPrincipal();
    }
}
