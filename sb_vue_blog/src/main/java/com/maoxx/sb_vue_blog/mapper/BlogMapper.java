package com.maoxx.sb_vue_blog.mapper;

import com.maoxx.sb_vue_blog.entity.Blog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 关注公众号：MarkerHub
 * @since 2024-07-05
 */
@Mapper
public interface BlogMapper extends BaseMapper<Blog> {

}
